% ח

עתה אספר מה שקרה לי עם הבתים שקניתי באלטונא. וכבר זכרתי למעלה מה שאירע כשקניתי הבית הראשון בית דירתי ועשיתי עליו הוצאות רבות מאוד; כפלי כפלים כנגד הקרן שעמד לי בתחילה. והנה בשובי מאמשטרדם מבריחתי, מצאתי שבנה שכן נכרי ביתו והשיג גבולי וכינס קצת מרשותי לרשותו להרחיב בניינו החדש ושתקתי. שוב הוסיף עוד לקחת בחזקה בפני מקום בית-כסא שבחצרי הנוגע בבנייני וסתר הבית-כסא ולקחו אליו וגדר אותו בפני ועשאו ב״כ לצרכו; לא יכולתי לסבול החמס יותר והוכרחתי לתבעו בדין בערכאותיהם ונמשך עסק המשפט שנה שלימה. אח״כ יצא הפסק דין שמחויב להחזיר לי הב״כ ולהניחו על חזקתו הראשונה וכן עשה. עם כל זה עדיין עיכב החצי המקום שלקח בעוול ולא החזירו והוצאות שגרם בדין כעשרים ר״ט הלכו לאיבוד.

בשנת תקי״ד[^1] קניתי עוד איזה בתים שהיו נוגעים בנחלה שלי ליראתי משכן רע וגם חשבתי להיות לי קצת משען לחם; באשר חזרתי על כל הצדדים להעמיד לי איזה מעמד פרנסה ולא מצאה ידי בעסק מו״מ עוד. וכבר נזכר למעלה מה שהיה עם ר׳ ליב לונדין והמעות שלויתי על ביתי הלכו לטמיון ונזהרתי בעצת חז״ל: לעולם ישליש אדם מעותיו[^2]; על כן הסכמתי לקנות הבתים הסמוכים אלי ותקעתי בהם במעט כל כספי אשר בידי, ט״ו מאות שו״ק ועדיין היו חסרים לי ארבעת אלפים מעות הראשונות, וחצי שנה ביקשתים ולא מצאתים, כמדומה בסבת אויבי חנם שהבאישו ריחי אצל כל בעלי כיסים נכרים המלווים מעותיהם על בתים, וכשבאה הזמן ולא השיגה ידי הוצרכתי לבקש הלוואה לפי שעה מאוהבים שלי. והרבה התחננתי וטרחתי עמהם להצילני מזילותא דדינא והוצאה לריק וכבדה אזנם משמוע, אף שבאמת מילתא זוטרתא היא לגבייהו[^3], כי היו לפחות ארבעה קצינים מסוימים שביקשתי מהם דבר זה הם: הקצין פרנס ומנהיג ר׳ גרשון כץ שי ופרנס ומנהיג ר״י הויזן שי׳ ופו״מ ר״מ גיסקא והמנהיג ר״י געסינגען ע״ה. ורציתי לעשות עמהם דרך היתר עיסקא ועל זמן קצר עד שאמצא מלוה נכרי, ולא שמעו אלי; כולם תלו באחרון שבהם הנ״ל כי הוא היה עתיר פומבי ומרא חיטא[^4] שהכל צריכים להמציא מעות בעין אצלו ואצלו הוא דבר קל ומצער הוא, מה גם על משכון בטוח כזה בעזה״י. מ״מ כל הבקשות והמליצות ממני ומאחרים היה בחנם, כי היה אדם קשה חוץ מכבודו. והניחני בצער הגדול הא, שכמעט היה קרוב להפסיד מעותי שתקעתי בקניין זה כבר - מלבד הבוז והקלון ושמחת המתנגדים לי - עד שראה כי באר מים עד נפש, שכבר עבר מועד הפרעון ונתבעתי לדין; אז נכמרו רחמיו אבל בכעס וקצף אמצי לי׳ נפשיה לתת לי שאלתי[^5] בפעם הזאת, אחר שגרם הוצאות של חנם כעשרים ר״ט, אז נתנו ארבעת אנשים הנ״ל בשותפות סך הנ״ל על רווחים ארבע למאה ופרעתי להם האלף ר״ט ונשארו מהלוואתם הללו אלף שו\׳ק על ארבע גברים הנ״ל בלי נתינת רווחים.

בפרק הנ״ל נפלו שורת אבנים מראש כותל בית דירתי בצד הרחוב ועשיתי הרצאה חדשה רבה על ביתי לחדשו לגמרי מבית ומבחוץ ולהניח בצדי הגג שעל הרחוב מרזב אבני מחצב עם מעקת ברזל יפים, תחת מרזבי עץ ומעקה שהיו לי קודם זה; באופן שעלו עליו לבד במאה ר״ט הוצאה חדשה; מלבד שהוצאתי גם על הבתים שקניתי מקרוב לחזק בדקם כל הצורך. והנה בשתי שנים הראשונות אחר קניין הבתים האלו היתה להם הכנסה כהוגן תודה לאל, שהייתי יכול לפרוע ריבית מקרן ראשון בזמנו ולהותיר בעד קרני כארבעה וכחמש מאות שו״ק לשנה, אחר כל מה שהוצאתי עליהם בתיקון ובמנת-המלך מאחוזת נחלה[^6]; באופן שבאותם שתי שנים היתה לי הכנסה ממעותי שתקעתי בקרקעות הנ״ל כחמש או כארבע מאות שו\׳ק לשנה בשופי. אך בשנת תקי״ז וחורף תקי״ח נפסדה הרבה מהכנסתי הלז. ה׳ ימלא חסרוני וישלח ברכה בביתי ובמשלח ידי ויחזק כוחי ואוני והוני / לעבודתו ית׳ לכבד ה׳ מהוני וממוני / ולהרבות מצוות וצדקות כחשקי. הוא יתעלה ימלא משאלות לבי לעשות רצונו!

בחורף תקי"ט[^7] מכרתי חמשה בתים שקניתי לפני ארבע שנים בלי היזק תודה לה׳. אחר עבור ארבע שנים מיום שנעשה השוד והחמס הנ״ל בביתי, בו בפרק היה מוזע יוליוס לפלטה גדולה (אחר שכבר חלה כשנה תמימה באלטונא ר״ל משמן בשרו רזה). הלך לדראון מאלטונא לפרידיציע. מקום מקלט הבורחים מפני נושים. לפני תשעה באב תקי״ט ברח מכאן הנבל עם סך ממון רב ועצום שעשק ליהודים ולנכרים. באותה שבוע ביום שלפני בריחתו לקח בגזל סך מסויים מיהודים בהמבורג העוסקים בחליפין והונה אותם. בהראותו להם כתבים מזויפים מסחורות הרבה שיש לו לקבל דרך ים קרואים קאנסומענטן[^8] ועל סמך זה האמינו לו כמה אלפים. גם עשה חתימות מזוייפות בכתב-חוב לנכרי אחד באלטונא. גם עם קרוב לחמישים אלף שו״ק קוראנד[^9] מעות מזומנים מלבד מה שחייב לסוחרים בעד תבואות. כן יאבדו כל אויבך! אח״כ שלח לי הנבל מענדל שפייאר איגרת בבקשת מחילה בהפצרה גדולה.

[אכן הצר הצורר הגדול לעשות רעה עם היהודים ולאמונתם. אויב ה׳ ועמו ותורתו, המין הרע התעוב והעוכר ישראל, אשר לא קם כמוהו מעולם לרמות ולטמא בתולת ישראל באפיקורסות בסתר ולהרים יד בתורה ולעשות ביד רמה ובזרוע חשופה למרוד באל עולם ולהתהלל באלילים - לא לבד הולך עד עתה בקומה זקופה כלפי שמיא בחוצפה], [אבל] גדלו הצלחותיו הזמניות מאוד מאוד.

בקיץ הלז השיא בתו הנזכרת באגרת שנייה להרב מלוטרינגן, אחר הייאוש מהיות לאיש ונישאה לבחור רך בשנים עם עושר רב ורץ לדרך מהלך בימי הזיקנה קודם ושב הנה; השיא גם נכדו בן בנו הגדול נאטש[^10] לבתו של אליהו ירוק בכבוד גדול ועשה הנישואין בכיוון יום קודם תענית שבעה עשר בתמוז, (שהוא יום טוב של ש״ץ שר׳'י. באופן שמשתה הקרובים היה בליל התענית.] וכל שבעת ימי המשתה נמשכו לימי בין המצרים בשמחה גדולה. וביום כ׳'ד תמוז עשה יחיאלכי וואלף סעודה גדולה עליו ועל בניו ומחותניו ושתו לסבאה מאוד.

ובאותו פרק שב בנו הבער וואלף עמו מן הדרך והביא עמו עשירות מופלגת עם חפצים יקרים, כסף וזהב וסגולת מלכים לרוב כפי המפורסם בקול גדול רלא יסף מהעושר הגדול שהביא בידו. גם קנה מיד אחוזת נחלה באלטונא; בתים עם גן גדול. ומשנכנס אב הלז הגדיל השמחה. עקר אילני פירות רבים וחשובים שלא היו דוגמתם באלטונא, טובים ומרובים עקרם ושרשם ואבדם על מנת לעשות לו במקומן נטיעה של שמחה[^10א] וחשק מאילני סרק ואבורנקי[^11] של מלכים בימי אב. גם קבע צורות חצובות מאבן אלבאסטר[^12] לקבוע בהן. וכן קבע הנער דירתו אז בשבת חזון חינך ביתו החדש הלז. והיה האב [^הצורר] אצלו על משתה חינוך הבית במוצאי שבת קודש הלז ששתו והשתכרו ביין לרוב, התגעשו והתהוללו. ועוד יותר מזה נשמע ונראה שקבע בית מדרש ועשה לראש משה דוד בעל שד[^15]; זה ילמוד שם כלומר חבלה[^14] עם תלמידים בחורים שהולכים ומתוועדים שם בכל יום לעסוק בחבלה[^13א] עם בעל שד הא אשר הלבישוהו אייבשיצר ובנו משי שנים ומאכילים אותו מעדנים ומרווים עסיס ותירוש ויראי ה' כואבים ושוממים על חילול השם הגדול, שבעים לענה ורוש. אנשי הכת הזאת מצטדקים ואומרים שבצדקתם ויושר לבבם עשו להם החיל הזה והנם בעינם הפתאים טובים וישרים; ואנחנו הזכאים דורשי ה׳ ומבקשי כבוד תורתו ומאמינים, החרדים על דברו ושוממים על חילול השם, בעו״ה נחשבים בעיניהם טמאים וחטאים / משוגעים, שוטים ופתאים / אוי לדור שכך עלתה בימיו! על זה ידוו כל הדווים שנהפכה השיטה: עליונים למטה וכיעורים בשמי שמיא; ממש העלה לשמים שיאו, בין כוכבים שם קינו[^14] וממעל לכוכבים הרים כסאו. בשנה שעברה הבאתי לדפוס ספרי "שימוש" וכמעט שנגמר נכנס רוח עושים להפועל משה בון ולא אבה לגמרו וגרם לי צער גדול, כי עשיתי על זה הוצאה חדשה למען ה' ולהקים תורתו, שלא תהא האמת נעדרת / לבל תהי המינות גוברת; וכמעט בא אל קיצו סמוך לגמרו עצורו ועדיין ועדיין מונח חסר. עד ירצה ה׳ פעלי, ה׳ יגמור בעדי / יחזק ידי / מעשי ידי אל תרף! בתחילת מרחשון תק״כ[^15] בא אלי מכתב מהר״א אמשטרדם השם יאריך לשנותיו מבקש שאבינהו אם מיתר לבטל ולשרש כת שבתי צבי ולהתאמץ להשיב ביחוד על-אודות עלילת דם שהחזיקו וקיימו מרורים כת ש״ץ שר״י להינקם מישראל על מה שרדפו אותם, כי על כן חרדו ועשו רעש גדול בפולין ונתחדשה גזרה. ה' יציל עמו מכל צרה! בתחלת שנת תק״כ נתפס יאקוב ראטשילד השודד ביתי; ואשר עשה ביתי אפותיקי על דבר זה ועמד בבית הסוהר כמה שבועות והוצרך לשלם אלף ר״ט ושוחרר מבית כלא. בטבת שנה זו היה לפלטה גדולה ועצומה הטיפש שמשון מורנא באמשטרדם, אשר קם עלי לשטן וארב על החיות. זה כמה עשה רשעה גדולה מאוד בעסק של אייבשיצר והשיב ה' גמולו בראשו. בו בפרק הגיעני מכתבים על אודות הנער המנוער בן זקן וכסיל הנ״ל ההולך בשטות אביו שותא דינוקא[^16] בשוקא וכתבו אלי מגלוגא וממקומות אחרים ממעשיו המכוערים והמקולקלים ביחוד במדינת מעררין ואונגארן בחן וזרע דעותיו שם.

בעזרת השם ית׳ ארשום שמות החיבורים שחנני השם ית׳ בדפוס ובכתב. הוא יתברך יחנני להוציא אותם לאור האמת, כאשר ברוחי אתי להגדיל כבוד התורה ולהאדירה וביחוד לקיים את דברי התורה ולבבות עמודים וחיזוקים לתורה ואמונה.

ראשית: הוצאתי לאור הדפוס בעזה״י ספר "לחם שמים" על שני סדרים עם קונטרס "בית הבחירה[^17]". גם חיברתי בס״ד מהדורה קראתי "משנה לחם" גם "לחם ניקודים". שנית: הדפסתי ספר "איגרת ביקורת[^24]". שלישית: ספר "שאילת יעבץ" עם קונטרס "עשרה הלחם" בתשובות שנשאל מעמדי בספר "לחם שמים", ח״א[^25]. רביעית: "סידור תפילה", שני חלקים הנקראים: "עמודי שמים" ו״שערי שמים". חמישית: ספר "מגדל עוז" ו״ברכות שמים" ששית: חלק שני מספר "בית מידות", עשרה בוגין כלתי ובאמצע נטרדתי במלחמת התנין. השם ית׳ יזכני בחסדו להשלימו. שביעית: "עץ אבות" ושלושה קונטרסים דרשות: א. "יציב פתגם", הספדו של אמ״ה ז״ל. ב. "שמש צדקה". ג. "שאגת אריה" להספיד גיסי הרב מאמשטרדם ע״ה, מלבד קינה על-אודות גירוש מפראג.

ואלה הספרים שהדפסתי במלחמת התבין: א. "תורת הקנאות". ב. "עקיצת עקרב[^34]". ג. "קיצור ציצת נובל צבי" עם הגהותי ותשובותי[^35]. ד. "סדר עולם רבא וזוטא[^36]" עם קונטרס נגד כת ש״ץ בפודוליה. ה. "שבירת לוחות האון" נגד חיבור אייבשיץ לוחות העדות[^34]. ו. ספר "שימוש" כולל שלשה חיבורים והשם ית׳ ברוב חסדיו יראני לרשום כהוגן[^35].

ואלה שמות הספרים שחברתי בעזרת השי״ת אשר הם בכתובים עדיין: א. "לחם שמים" עם חלק ב', עם "משנה לחם" ולחם בקודים כנ״ל[^36]. ב. "מור וקציעה" על טור אורח חיים ו״הלכה פסוקה" כדינים והלכות השייכים לספר תפילה שלי ולא נכללו בו בפרט[^47] ג. "שאילת יעבץ", חלק שני. "קולן של סופרים[^39]". ה. "הקישורים ליעקב"/ דרשות[^43]. "גלעד" על התרגום[^44]. ז. "אם לבינה" ו״אם למסורת" על תנ״ך[^45]. ח. "משכנות יעקב", הגהות על מדרש רבה[^46]. ט. "עזר אור", על התשבי ושאר ספרי דקדוק שהגהתי בעזה״י[^47]. י. "ליקוטי אורות" ע״ד הקבלה וסתרי תורה[^48]. יא. "מטפחת ספרים", כמה חלקים[^49]. יב. "מטפחת סופרים", נוספות על ספר יוחסין ושפתי ישנים[^50]. יג. "על התפילה ועל הברכות", ליוצאי חלצי. יד. "זכרון בספר". טו. "בדק הבית והמגדל". טז. חלק שלישי מ״בית מדות", כתב גדול[^54]. יז. "מגילת ספר" הלז.

ואלה שמות החיבורים בעסק הריב לה׳ המונחים עדיין בכתב: א. "איגרת פורים ומלחמת התנין". ב. "עדות ביעקב". ג. "בית יהונתן הסופר". "פתח עינים". ה. "תשובת המינים". ו. "שימוש ז. "הלכתא למשיחא". ח. "עקרב בית הבודה". ט. "אגרות שום" כמה שכתבתי על העסק ביש. י. "יקב זאב" על אודות בן זקן וכסיל. יא, "רסן מתעה". יב. "צעקת דמים וחסן כפון".

שילהי תק״כ נתעוררו הבחורים להעיד על מעשה אייבשיצר ובנו ואז התחילה המכה בנשים היולדות שנית. ובתחלת שנת תקכ״א[^61] הלך בן כסיל אייבשיצר לדראון ולא ישוב; ולימים מועטים הלך ג״כ המשרת שלו שהניחו פקיד על ביתו וחצרו; הלכו לפלטה ולא שבו עוד והניחו סרחון גדול של חובות שעשו אצל יהודים ונכרים. כל אחד הפסיד כעשרת אלפים שו״ק; כלו ממונם / כוחם ואונם / בבניינו של הנער וגבו ביתו וחצרו ומכרום עם כל הנמצא בהם כמבואר ב״יקב זאב" וב״שחוק הכסיל"[^62]. ועדיין המעות מונחים מפני שהנער עשה שם במעררין ובווינא ג״כ חובות הרבה ונתפס שם בקלון בהעלישוי. ואחריו נאטש עשה שטר חוב מזויף מקודם כאילו חייב לאדם אחד בפראג סך כזה, על כן באו במשפט כאן לעקל החובות. בשנה זו בפסח נמכר הבית והגן של הנער בדמים מועטים.

בו ביום היה לפלטה גדולה מראשי הפרנסים באלטונא ר׳ יוסף פופרט, אחד החתומים שביקשני למסרני למלכות. באותו פרק בא לכאן שמשון מורנא מאמשטרדם להחיש מפלט וסבר ג״כ לגבות חובותיו מיוזפא הירש ויצא בפחי נפש לאמשטרדם ומיד בשובו נפטר שם. תהא מיתתו כפרתו! ור״י פופרט ביקש ממני שאניחהו לילך לבית הכנסת שלי שמפני הבושה לא יכול לבא לביהכ״נ הגדולה ונמצאת לבקשתו והתמיד הליכתו בביהכ״נ שלי עד אחר החג. בשנה הנ״ל ט״ו תמוז נתעורר בי מיחוש גיד הזהוב שהיה מושך דם כמעיין והיו רוצים שלא אצום בו ביום ולא נטיתי אוזן לרופא שאמר שיש סכנה בדבר. אמרתי: יעבור עלי מה לא אוכל לאכול בחג של שבתי צבי! כי זה וודאי לנסיון בא עלי. ונתחזקתי והשלמתי התענית בעזרת השי״ת ושפע הדם התמיד בקצף גדול משך שלושה שבועות עד אחר ט׳ באב וכבר לא היה בי כוח לעמוד על רגלי, עם כל זה גמרתי תענית תשעה באב אע״פ שהוצרכתי למשענת בלכתי אנה ואנה וכמעט לא נשארה בי רוח חיים. והשם ית' נתן ליעף כוח לעמוד גם בזו. אחר זה החזירני לאיתני. יהי שמו הגדול מבורך!

ביום הזה נתן משה קובעס כתוב וחתום בידו בברעמען פטורים על עסק המשפט אודות ספר "עקיצת עקרב" ושיוחזרי לי החביות עם מה שבתוכן, שגזל אותי לפני שש שנים ובבוא הכתב תבעתי ליעקב ראטשילד ונפל למשכב ונעשה חפשי ונתבעה אלמנותו ורצתה לפטרני ג״כ, ועמד אייבשיצר והלך בעצמו לערכאות ואמר שהוא איננו מניח להחזיר לי החבית, כי גם "כבודו" לקה על ידי והעמיד לו מליצים ועכבו יותר מחצי שנה, אבל לא הועיל לו, כי יצא דבר המשפט שאם רוצה להגיש עצומותיו לנגדי צריך שיעמיד ערב מספיק ובטוח על הרצאות והיזקות; הלך וביקש אצל מאהביו שיכנסו בערבון בעדו ואין שומע לו. ובראותו כי אין לאל ידו והפסיד מעות בחנם, גם היה בחולי, אז נשלם הדין ונפסק שיוחזר לי הנשלל מביתי. זה אירע בחג גדול של ש״ץ, י״ז תמוז תק״כ, בפרשה: לכן אמור הנני נותן לו את בריתי שלום[^68]; והחזירו לי החבית שלי שבוע שלאחריה ג״כ בחג של ש״ץ בלי נפקד ממנה כל מאומה. ככה יראנו ה׳ חסדיו ויזכנו לנחמה שלמה במהרה בימינו אמן!

שנת תקכ״ג[^64] נודע לי שבתי אסתר ע״ה נפטרה לעולמה. יהי שמו ית' מבורך! בשנה זו הוצאתי לאור ספר "שימוש". אחר זה זיכני השם ית׳ לימי בינה דמנהון דינין מתערין[^65]. מיד במחלה אחר חג העצרת בפרק מולדתי התעורר כי הזלת גיד הזהוב בשפע ולא חשתי לו מאומה והודר שנתי מעיני בלילה בכתיבת תשובה לשואלי דבר תורה, הוגעתי בלי שום דאגה אבל הייתי מחמת זה בסכנה למחרת, כי גרם לי ביום סיבוב הראש ולא יכולתי קום על רגלי. איזה שעות הוכרחתי לישכב במטה ונעשה רעש בעבורי כי הוצרכתי קצת לתרופה. כמה ימים הייתי בייסורים עצומים כי לא יכולתי לישן בלילה מחמת הייסורים עד שחמל ה׳ עלי מיהר צועה להיפתח[^66] ולא נתנני לרדת שחת. ושמעתי מאוד תנועת הדפק והקשתו במוחי חזק כל כך עד שהיתי חושב שיש איש מכה וחוטב בגרזן סמוך לאזני, ואביט כה וכה ואין, עד שהבינותי שההכאה וההקשה במוחי. אחרי שובי לבריאותי נפלה בחולי בלאטרן בתי הילדה היפת תואר ויפת מראה וטובת שכל שנשאה חן בעיני כל רואיה[^67] וכבד חליה מאוד והלכה גם היא לעולמה, אחר שהשלימה ארבע שבועות שלימות, אחר שחלתה כמה פעמים; וכל פעם חזרה לבריאותה עד שהגיע קיצה ע״י חולי אבעבועות וגרמה לי יללה גדולה. זכרתי משפטי ה׳ מעולם ואתנחם!

קרוב לסוף שנה זו קודם ראש השנה תקכ״ד[^68] נעשו בריחות[^69] גדולות ועצומות בשלוש הקהילות, ממש לא נשאר איש מהחלפנים ובעלי כיסים העצומים שלא היה לפלטה גדולה מלבד מתי מעט; ורוב הבורחים ככולם היו מכת שונאים שלי, רק שנים-שלושה גרגרים, אנשים בחורים מתחילים אשר היו בצד שלי נכון בגחלת זו; ונהרס מצב משא ומתן של כתב חובות ונאמנות. הבערש[^70] נפל עד היסוד בחצי שנה זו. עד היום לא חזר לאיתנו כבראשונה. אח״כ הלך לפלטה הירץ האלי פרנס קהל אלט, שהיה גם אחד מראשי מתנגדים שלי לפנים.

אחר החג השלים אייבשיצר חיבורו כרתי ופלתי ועשה ממנו דורונות בכל צד ופינה בכאן ובכמה מקומות; ואומרים שהרוויח בהם כמה אלפים, אעפ״י שהיה לשחוק וללעג ועשו עליו השגות אפילו הנערים והבחורים. בו בפרק נפל [התועב] בחולי כבד.

אמרו שהיה אחר יאוש כשלשה ימים ועשה מהומה גדולה אצל כת שלו. אכן אני אמרתי לאנשים שלנו: התפללו בעדו שלא יפטר כך מן העולם בכבוד בלי שיעשה תשובה חלילה וירגיז שוכני עפר; גם יעשו ממנו תועבה, לכן העתירו בעדו שיעשה תשובה ויהא לו תיקון ואם לא ישוב שלא יבוא לידי קבר ישראל; ובכן שב לחייו שאינם חיים; ועדיין הוא מאריך ברעתו ~רעוד~{ועוד} השעה משחקת לו שחוק הכסיל; ועשה עוול וחמס לכמה בני אדם הצועקים חמס מפניו בענייני משפטים שעושה כרצונו נגד הסכמת הבית-דין והקהל ואינם נענים, עושה כרצונו ולית דימחה בידו[^71].

בו בפרק באו הנה משולחים מחאטין לצורך פרנסת הגרים של רואיה שנלוו עליהם זה ארבע שנים - כנזכר בספר שימוש -שאז התחילו לבוא כארבעים איש ומן אז והלאה הולכים ומתרבים מדי שבוע בשבוע ונוספים לחמישים ומאות; סובבים ומקיפים בדרך עקלתון ארוך עד שבאים לחאטין, והמקום צר קטן מלהכיל אותם, והוכרחו לשלוח שלוחים לקבץ נדבות הקהלות באשכנז. וכבואם הנה סרו אלי; מיד באו לביתי ולא אבו ללכת אל [התועב], בידעם והכירו [תועבותיו] אשר עשה וקשרו קשר על ה׳ עם הארורים כת ש״ץ בפרדוליה, כי קרובים הם אל החלל[^72] הגדול הנעשה שם. גם סיפרו שנמצאו שם [כתבי המינות] הרבים מעשי ידי המין, והרבה מהם שרפו שם בק' חאטין ונשארו ג״כ ביד מי ומי ששימשו אותו; [^המין המתועב בטומאה] גם יש מקרוביו שם שהיו בידיהם כתביו והפיצום שמה; בגלל הדברים האלה לא רצו האנשים האלה להסתכל בדמותו [דמות אדם רשע], שלמים הם ונאמנים בשליחותם ואמרו: אנן שלוחי מצווה ולא שלוחי עבירה! ואעפ״י שהרבו עליהם רעים לרצותם שילכו אליו ואוהביו מיראים אותם שאם כה יעשו יהא להם תועלת גדולה בעסק שליחותם, ואם לאו יפסידו, לא השגיחו השלוחים בדברים הללו ולא יכלו כל אוהביו להחזירם מדעתם ועשו קידוש השם גדול שלא נעשה כמוהו בכל שנות הריב לה׳. בשביל כך לא נתנו להם מקופת אלטונא אף פרוטה אחת; אך קהלת המבורג עשו הפעם כטוב ולא פנו אל קהלת אלט. ולא מנעו עצמם ממצווה זו הרבה ונתנו מכיס הקהל עשרים דוקאטין שפעציא לצורך הגרים. מלבד זה העמדתי להם שני בעלי בתים נכבדים בקהל אלט. ושנים בק׳ המבורג שיסובבו על הפתחים פתחי נדיבים לצורך הספקת הגרים. באופן שעלה הסך המקובץ בכלל עם הנ״ל כשמונים דוקאטין; נמסר בידי ושילמתי בעד פרנסת השלוחים שהוכרחו להתעכב פה מן קודם חג המצות תקכ״ד עד אחר חג השבועות; ונשארו בידם כחמישים דוקאטין לשלחם ליד הממונים על הגרים בחאטין.

בו בפרק בא הנה חתני ר׳ מענדל ועמד בביתי כל ימי ניסן הלז; אחר נסע לברלין ואחר איזה ימים שב לכאן, ושלחתי על ידו סך מסוים מחיבורי, בערך ארבעים ר״ט כפי המקח שאני נוהג למכרם. עוד זה הלך וזה בא: חתני ר׳ אליעזר עם זוגתו בתי נחמה תחי׳ בחשבו שתשוב לבית אביה כנעוריה, כי שנא אותה ורצה לפוטרה בגט פטורין. ונדמו בעיני שתי הבנות הללו שנשתדכו בבת אחת בקול גדול כחרס לוחות שנשברו. זה היה מעשה בתום שנת דינין הנ״ל; ונכנסתי לשנת חיים[^73] בחסד ה׳. יהי רצון מלפניו שאחדש כנשר נעורי ויזכני ברחמיו ורוב חסדיו, לשנות חיים ארוכים וברוכים לעבודתו וליראתו ולתורתו, תחת שנות הצרות והרעות אשר עברו עלי בלי למצוא מנוח ונופש. אף כמעט רגע לא טעמתי טעם חיי קורת רוח בעולם, לא בגוף ולא בנפש ולא מחוץ לגוף; לא בעמלי ויגיעי, לא בכוחי וראשית אוני; לא ראיתי אפילו שמחה אחת של יוצאי חלצי הנשואים, רק שבעה ברעות נפשי. יגעתי באנחתי / מנוחה לא מצאתי / בה׳ בטח לבי, אשר הראני צרות רבות ישוב לנחמני / ומתהומות הארץ ישוב יעלני /; לשכון כבוד בארצנו; יזכני / לנוב בשיבה טובה ולעבוד אותו באמת בשמחת לבב על אדמת הקודש לראות בטובת בחיריו העושים מאהבה, כן יהי!

בשלושה לירח אב שנה זו חזר [התועב הזקן אשמאי] ונפל בחולי שיתוק פתאום. והיתה מהומה גדולה בביתו איזה ימים [ובסוף אלול נפגר התועב, גווע בבלי דעת, בלי ווידו וחרטה. ולא אעסק בפגירתו, יכתב עניין זה בנייר בפני עצמו.] על יומו נשמו אחרונים[^74]; עד עתה לא פסקה זוהמתו. הציגו מצבה על קברו. גם עשו ממנו דמות צורתו משוח ~בששר~{בשטר} והכסילים האווילים הנצמדים אליו מחבקים ומנשקים אותה. איש אחד הניח הנייר שבו הצורה הארורה בתוך חומש על מקום עשרת הדברות. למדן אחד מבני ליטא נשתגע על-אודותיו. כאשר הביא לו תלמידו קמיע מה שמצא אביו בתוך תיבה שקנה מאייבשיצר מעזבונו ונמצא כתוב בה שמשביע בשם שבאם[^75] צבי ובשם יונתן בן שיינדל. על דבר זה יצא מדעתו המלמד ההוא שהיה עדיין רך בשנים ועסק גם בחכמות חיצוניות והעמיק בידיעות יותר מהראוי לו; עד שהוחלש מוחו כבר קודם לזה עד שהכניסוהו בבית החפשית[^76] באלטונא. אבל לא נזהרו בו כראוי ולא שמו עליו משמר. פעם אחת יצא לחוץ וחפר קברו של אייבשיצר כמעט קט הגיע לארונו, לולי שראו ומצאוהו עוסק בחפירת הקבר ומנעוהו בכוה והחזירוהו לבית האסורים וכבלוהו. אחר נטלו הכבלים ממנו ועזבוהו לנפשו, כי חשבו שנתיישבה דעתו, שהיה אומר תמיד דברי קדושה ואמר כמה פעמים שרוצה לטבול עצמו. ויהי כי ראה עצמו בלי שמירה יצא ורץ עד מקום המים וטבל בהם אבל שקע בהם ורצה להציל עצמו לאחוז באיזה דבר - כאשר העיד נכרי מרחק שעמד שם - אבל לא יכול כי היה חלש ונטבע מיד; והביאוהו לקבר ישראל בכבוד; לווייה גדולה עשו לו סמוך לכניסת שבת קודש שנת תקכ״ט[^77].

בשבט התוועדו שלוש הקהילות על-אודות מינוי רב חדש והיו רבים חפצים להשיבני לנחלת אבותי וכשעמדו למניין, לעשות רשימה מהראויים לאיצטלא[^78] זו נעשית מריבה ביניהם על-אודותי וכמעט היה הרוב על צידי; וכך היה בוודאי; אם היו עושים דבר זה בגלוי לא היה אדם עולה אחר עלי, להתמנות רב בשלוש הקהילות בפעם הזאת. אבן התחכמו שלושה מאויבי הלא הם: ריש בריוני יחיאלכי וואלך, יעקב שלזינגר, הירץ שטאר ובטלו הוועד ההוא. אח״כ התחבלו להפיק זממם, לדבר על לב הנועדים לשוב להתוודע מחדש על עסק זה ולקבץ דעות בהסתר והעלם מכל וכל; דהיינו ע״י שיעשו מיני עיגולים שונים, באופן שהאחד יורה על הן והשני על לאו. ועשו שתי קופות מהעיגולים כדי שיקח כל אחד מהנבררים לחוות דעתו; באופן שיקח האחד מהעיגולים שירצה בהסתר בלי שיראה אדם מאיזה הוא לוקח ומניחם בקלפי. אח״כ ימנו העיגולים ויראו מאיזה הרוב ונתרצו יחדיו בזה. ובתוך כך הלכו המתנגדים הנ״ל ופיתו והסיתו קצתם עד שסרו למשמעתם אחד או שנים, אולי ברצי כסף, נתרצו להם לחזור בהם ולבגוד בי ולא דאגו מעתה, כי על כל פנים לא יוודע מי הוא אשר חזר בו מדעתו הקודמת ומיאן בי ועשו כן והצליחו מה שרצו לסלקני מן מניין הראויים. מעתה חשבו שעלה בידם כבר ג״כ לבחור ביוסף מוקיר שבתא שטיינהארד אב״ד מפיורדא[^79] וחשבוהו כאילו אחר מעשה הוא, אבל טעו כי אחר זה כשעשו בוררים לקבל הרב החדש; אעפ״י שהיה נדמה לכול שהרוב היה לצידם, אכן מאת ה׳ היתה שקצתם ביקשו לעמוד על דעתי להיכן נוטה ועל כן הוצרכתי לתת דעתי והסכמתי להרב מבראד[^80] מכמה טעמים עד שעלתה בידינו ונבחר זה האיש לרב בג' קהילות על ידי בס״ד.

בחודש ניסן נתקבל בני הרב כמ\׳ו משולם זלמן נר״ו לרב ואב״ד דק״ק המבורג שבלונדון[^81] ג׳׳כ ע״י השתדלותי ופעולותי בעזה״י זמן-מה. וכבר היה אחר היאוש, כי היו לו כמה מתנגדים מצד בית הקהל דוקספלעיס[^82] שנפרדו מהם וקיבלו לעצמם אב״ד ר׳ טבלי שיף מפראנקפורט דמיין; אבל מאת ה׳ היתה זאת היא נפלאת בעינינו שלא הועילה כל ההתחכמות וההתנצלות הצד המתנגד לבטל מינויו; גם אחר שכבר נתקבל בהכשר כהוגן קשרו עליו קשר וכתבו לו איזה יחידים מן הצד הנ״ל איגרת מרד ומיאון בהתראה שלא יבוא עליהם כי יסרבו בו; ונעשה ע״י הסרסור לדבר עברה תלמידו של איש אחד שישנו שם שהתעצם בזה. גם שלח איגרת אוגרת ומסוכסכת כזו על הפאשט, כסבור ליראני ולבהלני בזה כדי שאעכב בני שלא ילך שמה. אך הצד אחר רצו בו; המה הרהיבוני וזרזוני על כמה שלא אשגיח במעשה תעתועים של המתנגדים וכן עשיתי. והזהרתי את בני שלא יפנה לדברי האגרת המכוערת ועשה והצליח ת״ל. בא עד הנה ושבת אצלנו בחג השבועות הלז וכל הנכבדים דשלוש הקהילות עשו לו כבוד ומצא חן בעיני בל רואיו. ונסע מזה והגיע למקום חפצו וכסא כבודו בחצי תמוז ונתקבל בכבוד גדול ושמחו בו כשמחה בקציר. העם ההולכים בחושך ראו אור גדול[^83] והפליגו ממנו מאוד. והגיעני משם אגרות שגם האויבים נהפכו לאוהבים וששים ושמחים בו כמשוש חתן על כלה[^84]. יהי רצון מלפני בעל הרחמים שיצליח ויעלה מעלה מעלה לזכות לעצמו ואחרים עמו ויזכהו בבנים וישמחני ביוצאי חלצי בכלל ובפרט כן יהי רצון; אמן!

בקיץ הלז הגיעני מכתב מהרב דפרשבורג על-אודות נתן ארהאלץ תלמידו של אייבשיצר, המורה בשטמפן שהקים אשרה[^84א] לרבו וביקש ממני גביית עדות מהבחורים הרשעים מקויימת[^85] ולא עלתה בידי. והגיע הנה הרב החדש כמו״ה יצחק שהיה לפנים אב״ד בבראד. בשבעה לתשרי שנת תקכ״ו העיר ה׳ את רוחי לקנא קנאתו לשרש אחרי עבודה זרה, אחר שיקוץ שלו מאחר שהניח שרשים רעים למורים בישראל. ובכן קמתי ונתעודדתי ב״ה בבהכ״נ שלי ביום צום לגרש מקהל את הנער וואלף אייבשיצר ואחיו ואת ליב רכניץ, נתן ארהאלץ, מאיר פרוסטיץ, משה פודהייצר ולא חרץ איש לשונו, אף אם אח״כ בהישמע הדבר ונתפרסם חרה לאנשי אייבשיצר; מ״מ נאלמו בעל כרחם, שמו יד לפה אך חשבו שלא יעשה שום רושם בעולם, באשר אין איש מתחזק עמדי. גם לא האב״ד החדש אשר הוקם על פי שחשבתיו ג״כ מקנא קנאת ונוקם נקמת ה' במדינים הצוערים אותנו בנכליהם[^86], כי אמנם בא על החתום נגד אייבשיצר; והיא שעמדה לו להביאו על כסא הרבנות של שלוש קהילות; אחר שירד מכבודו בבראד, רדפוהו שם מאוד עד שהתחנן למחותנו הרב דהנובר להביאו לארץ אשכנז אפילו לרבנות קטן שבקטנים, והרב בהנובר עשה בכל מאמצי כוחו גם לפייסני ולהתרפס ברצי כסף להתחייב בעדו לתת לי הרב מבראד מחותנו אם יתקבל לרב בג' קהילות: חמישים אדומים דבר שנה בשנה, מלבד שאר הבטחות להוסיף ולא לגרוע - ונמצאתי לו. אך העיקר היה למען יהא נאמן לה׳ ויהיה מסייע לדבר מצווה רבה זו. אך כשנבדק נמצא גרף... לא די שלא עשה מאומה, כי ירא לפתוח פיו ולדבר מאומה רע עליו - מדאגתו פן יפסיד איזה מתנה ודורן אצל האנשים הדבקים באייבשיצר עד עתה; ולא די זה בלבד אלא שנתן יד להקים אשרה חדשה ונתן מעלת מורנו לנכד אייבשיצר, אעפ״י שמחיתי בידו באיום גדול ולא השגיח בי, כי היה האיש אוהב בצע וירא פן לא ימצא חן בעיני הנקשרים לאייבשיצר. אף לא רצה לתת לי מאומה ממה שנתחייב בעדו מחותנו הרב הנ״ל. אך לבסוף, מחמת רוב הפצרות הרב דהנובר התפשר עמדי; קיבלתי י״ב וחצי אדומים מהרב דהנובר וחמישים מהרב אב״ד; גם נתן לי שטר חוב לשלם קודם הפסח תקכ״ז[^87] הבעל". בכן החזרתי לו כתבי השיעבוד. גם התחיל לעצור ספק ואמר: מי יודע אם היה אייבשיצר מכוער כל כך? אף הוא אמר שהחתימה מידו נגד אייבשיצר יצאה מתחת ידו על פי תחבולת הרב מאמשטרדם בעד איזה הבטחה שעשה לו בעסק שהיה מסובך עם פרופס[^88] במעות שתקע בהדפסת הש״ס; היא שגרמה: לשון רכה תשבר גרם[^89] כי לא עינה מלבו. בקיצור על כרחו שלא בטובתו נשאר חמר גמל בין שני הצדדים[^90]; כשנמצא עם הצד של אויבי אייבשיצר דיבר גם הוא רעה למען לא יפול בעיניהם; אך בפני המתנגדים ירא לדבר מאומה כנ״ל. לבן לא יכולתי לפעול עמו דבר ונשארתי לבדי / אין מתעורר להחזיק עמדי; / אכן אלהי אבי היה עמדי / להחזיק בידי / לא אירע מרגשת פועלי און וחורשי רע בלבבם. בצל ידו החביאני / ובכנפי חמלתו הסתירני/.

אמנם הכרוז בבית הכנסת שלי עשה רושם. מיד תוך ארבע עשר יום נכווה בגחלת הכרוז ישראל שווערין, המורה לצעקה[^91] מחזיק באזני כלב פרוסטיץ הנ״ל. וחתרתי כל הקיץ שעבר, כתבתי לשווערין[^92] שיסירו האיסור מן העדה פן יהיה למוקש להם והיא לא תצלח! ולא אבו שמוע אלי. ויהי ביום הושענה רבה הלך למקווה ונכווה ברותחין ששפכה עליו משרתת הבית ונבושה עורו והיה בסכנה גדולה וייסורים מכוערין כמה שבועות ולבסוף נסדר לו רופא להקיז דם והעלתה המכה צמחים. וגם בזאת לא שם לבר לדבר ה׳ ועודנו מחזיק ברעתו לעזור לרשע ולסייע לדבר עבירה, להיות לו סורר ומורה הפושע פרוסטיץ.

בחורף הלז, שילהי כסלו, נמניתי אני לבורר של אישה קילה לגמור דינה ותביעתה מיורשי בעלה וישבתי פעמים עם הרב אב״ד ודיין ר׳ ליב יפה והדברים ארוכים כתובים אצלי בקונטרס בפני עצמו. בו בפרק חזר והתעולל בי מיחוש גיד הזהוב, כי נתקררתי אחר חימום גדול שהיה לי בישיבת הדיינים ובהיותי מושך דם כמעין ביושבי ביהכ״נ ובצאתי נפלתי לארץ מרוב חולשה והיתה בהלה גדולה בביתי ובקהילה. וה' חמל עלי ושב רוחי אלי. אך כמה שבועות לא שקטה רתיחת המעים והיה הדם כנחל שוטף, גם הוחלשו כוחי מאוד, לא הייתי בקו הבריאות כמו שני חדשים. אחר חזרתי לאיתני הראשון ת״ל חיי, בו בטח לבי ונעזרתי, הוא יתברך יחייני לעבודתו וליראתו.

בראש חודש אדר הגיעני מכתב על הפאשט מרעכניץ מקום אשר ליב סטשובר אסור שם למורה איסור והיתר; מודיעים אותי הכשרים שבקהל ששמעו ידעו וראו העתק הכרוז שנעשה בביהכ״נ שלי על האנשים הנ״ל ופחדו ורהו והיו רוצים לגרש הרב שלהם הנ״ל. אך קמו בני בליעל המחזיקים בידו וההבילו הכרוז, על כן כתבו לקהל דכאן לידע אם נעשה בהסכמתם וקבלו אגרת תשובה מפה שלא ידעו מזה הכרוז. אף הוסיפו לגזור עליהם לבטל האיסור בכל עוז. ואנוכי השבתי להם בכתב יד ארוך כיד ה' הטובה עלי, להעמיד האמת ולעשות סייג לתורה. השם יקנא [לשמו!].


[^1] 1754.

[^2] בבא מציעא מב, א... ושלוש בפרקמטיא, ושלוש תחת ידו.

[^3] דבר קטן לגבם.

[^4] עפ״י בבא בתרא קמה, ב, כלומר מפורסם בעושרו והכול צריכים לו.

[^5] המציא עצמו לי, ע״פ יומא פז, א.

[^6] מס רכוש.

[^7] 1759.

[^8] הזמנות.

[^9] כסף בטוח.

[^10] נטע אייבשיץ.

[^10א] דרך לגלוג עפ״י המאמר: "משנכנס אב ממעטים בשמחה". תענית כ״ו ב.

[^11] ארמון חופה.

[^12] שיש.

[^13] דרך לגלוג: בעל שם.

[^13א] דרך לגלוג. קבלה.

[^14] עפ״י עובדי ד׳.

[^15] 1760

[^16] מה שהתינוק מדבר בחוץ משל אביו הוא או משל אמו, לפי סוכה מו, ב.

[^17] וואנזיבעק תפ״ח.

[^18] יצא לאור באלטונא.

[^19] על דקדוק הלשון, ראה להלן: עץ אבות.

[^20] אלטונא תצ״ח.

[^21] שם תקה-תק״ה..

[^22] שם תקי״א.

[^23] גליונות.

[^24] אלטונא תק״ד.

[^25] אותיות יהנתן (אייבשיץ), הספר נאבד.

[^26] אמסטרדם תקי״ב.

[^27] אלטונא תק״ד.

[^28] אמסטרדאם תקט״ז.

[^29] נשאר בכתב-יד ונאבד.

[^30] אמסטרדאם תקי״ב.

[^31] על שער הספר זאלקווה, אבל נדפס בבית דפוסו באלטונא.

[^32] לר' יעקב ששפורטש, אלטונא תקט״ז.

[^33] המבורג תקי״ו.

[^34] אלטונא תקט״ו.

[^35] אלטונא תקכ״ח.

[^36] נשאר בכתב יד רוזנטאליאנא, אמסטרדאם.

[^37] כתב יד בודליאנא, אוקספורד.

[^38] כתב-יד מכללת קולומביא, ניו יורק.

[^39] כנ״ל.

[^40] כנ״ל.

[^41] קטעים מספר זה בהגהות מדרש רבה לר׳ יעקב בוכנר, פראנקפורט דמיין 1854.

[^42] הוציא וההדיר כותב הטורים, תדפיס מה״מעיין", ירושלים תשל״ב.

[^43] אלטונא תקכ״ח.

[^44] כנ״ל.

[^45] חלק נדפס ביוחסין, הוצאת פיליפובסקי לונדון תרי״ח.

[^46] הוצאת המהדיר, כותב הטורים, בהוספות לסידורו (צילום), תל-אביב תשכ״א.

[^47] ההדיר כותב הטורים, תדפיס מ״תרביץ", ירושלים תשל״ג.

[^48] נשאר בכתב-יד ונאבד.

[^49] נאבד בימי החיפוש על "עקיצת עקרב".

[^50] כתב-יד בודליאנא, אוקספורד.

[^51] אלטונא תקט״ז.

[^52] אלטונא תקכ״א.

[^53] אלטונא תקט״ו.

[^54] כנ״ל אמסטרדאם תקי״ח.

[^55] אמסטרדאם תקכ״ג.

[^56] נשאר בכתב-יד בגנזי צאצאו ר׳ שאול ישכר ביק ז״ל ונאבד.

[^57] נדפס בספר "התאבקות" אלטונא תקכ״ב.

[^58] נדפס בספר ״פנקס ועד ארבע ארצות״ לח. הלפרין, ת״א 1941.

[^59] נדפס ב״שמוש" הנ״ל.

[^60] נדפס בספרו של המהדיר "ר׳ יעקב עמדין האיש ומשנתו", מוסד הרב קוק, תשל״ה.

[^61] 1761.

[^62] ראה "התאבקות" עמ' כד, א.

[^63] פרשת פנחס.

[^64] 1763.

[^65] מלאו לו ששים וארבע שנים, לפי אבות ה, כא. בן ששים לזקנה, ואיוב לב, ט; וזקנים יבינו משפט. שמהם מתעוררים הדינים; ששים וארבע בגימטריא דין.

[^66] עפ״י ישעי׳ נא, יד.

[^67] עפ״י אסתר ב, ז.

[^68] 1764.

[^69] פשיטות רגל.

[^70] בורסה.

[^71] ואין מוחה.

[^72] עפ״י דברים כא, ב.

[^73] מלאו לו ס״ח שנים, בגימטריא חיים.

[^74] עפ״י ישעי׳ נד, יד.

[^75] שבתי צבי.

[^76] בית חולי רוח.

[^77] 1769.

[^78] מישרה.

[^79] ר׳ יוסף שטיינהארט, רבה של פיורדה (נפטר 1772).

[^80] ר׳ יצחק הלוי הורוויץ (נפטר 1767).

[^81] קהילת יוצאי המבורג בלונדון.

[^82] בית כנסת ישן בלונדון נוסד ע״י אנשי פראנקפורט דמיין.

[^83] עפ״י ישעי׳ ט, א.

[^84] שם סא. יג.

[^84א] לפי סנהדרין ז, ב; כל המעמיד דייו שאינו הגון כאילו נוטע אשרה בישראל.

[^85] כלומר עדות מאושרת.

[^86] עפ״י במדבר כה, יז.

[^87] 1767.

[^88] מדפיס באמשטרדם במאה ה-18.

[^89] עפ״י משלי כה, טו.

[^90] פוסח על שתי הסעיפים, על פי משנה עירובין ג, ד.

[^91] דרך לגלוג למורה לצדק.

[^92] עיר הבירה של מחוז מקלנבורג.
