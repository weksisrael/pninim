% שאלה מה

עמדין הת"צ

*ששאלת* אותן מדינות הנוהגות קלות בסתם יינם. אי ש"ד לברוכי עליה ולקדושי ואבדולי:

[^1]*תשובה* אתמהה מה זו שאלה. שכבר שולחן ערוך לפניך (בסי' קצ"ו) שפסק בפשיטות כהרמב"ם ז"ל שאין מברכין על דבר איסור אפי' דרבנן. לא בתחלה ולא בסוף. ומי יבא אחר המלך. וביחוד שפשטי השמועות וסוגיות התלמוד מסייעות לרמב"ם ז"ל. ואחשוב שהביאך עד הלום להכניס ספק בלבך. מ"ש בתשו' נקרא באר עשק שנתעסק בדין זה. (בסי' ק"ט) וכתב שיש לברך ולקדש עליו. מחמת טעם טפל ואפל לפי שתקנת הברכות לדעתו קדמה לתקנת סתם יינם ע"ש. שהביא ראיה לדבריו מההיא דפ"ק דתענית יחיד שקיבל עליו להתענות ב' וה' של כל השנה אם נדרו קדם לגזרתינו יבטל נדרו את גזרתינו. והוכיח מזה דתקנת הברכות דוחה ומבטלת לתקנת ס"י המאוחרת:

[^1] הוראה דהאי צ"מ בב"ע בענין ברכה על ס"י לקלקולא הס לא להזכיר חליל' לסמוך עליו לקולא וראייתו בטלה ושגיא' גדולה


*ושרי* ליה מאריה שנתן מכשול לרבים אשר לא יחכמו. כי בודאי היודעים ספר לא יטעו מכח דברים בטלים הללו. כי המה מהבל יחד וכלים מאליהם. ומי זה מחשיך עצה במלין בלי דעת ולא חש לקמחיה כלל. ונראה דלא ידע בשותא דרבנן. דידוע לכל בר בי רב דחד יומא שיפה כחן של חז"ל לעקור דבר מן התורה בשב ואל תעשה אפי' באיסורי כרת. ולחד מ"ד אפי' בקום עשה כדס"ל לר"ח בהאשה רבה דההוא גריווא הדרא לטיבלה. דחכמים מתנין לעקור דבר מן התורה כדי להעמיד גזרתם. וכ"ש כאן בנ"ד דחיוב הברכות (לפחות ברכות הנהנין) כל עיקרה אינה אלא מדבריהם. שפשוט מאד שיפה כחם לעקרה. וביחוד בשב ואל תעשה דלא לתהני ולא לבריך. שאין שייכות קדימה ואיחור לדבר זה. אפי' היה כדבריו שתקנת הברכות קדמה. מה שאינו מוכרח. שאפי' קדוש היום שסמכוהו על המקרא זכור זכרהו על היין. אינו אלא אסמכתא לדעת הר"מ והתוס'. דדבר תורה סגי בתפילה אי נמי על הפת. גם סתם יינם כמ"ש בפר"א פנחס גזר עליו אע"פ שחזר והותר בא"י לפי שעה כעין דברים אסורים שהותרו בכיבוש הארץ. א"כ איהו ניהו דקדים ודאי. גם אי אפשר ליהנות מכוס בלא ברכה. הילכך אין מקום לומר שיקדש עליו וישתה בלא ברכת היין שלפניו. דהא עיקר קידוש הוא בפ"ה דמיקרי קידושא רבא. חס ושלום דליעבד תרתי. ר"ל מצוה הבאה בעברה כפולה. שישתה ס"י גם לא יברך על הנאתו. ופשיטא דבין כך וכך עון חמור בידו שאם מברך אינו אלא מנאץ. ואם אינו מברך כ"ש שריחו נודף שגוזל ג"כ אביו ואמו ונעשה חבר לאיש משחית. הילכך אין תקנה לדבר זה. אלא אין לך אלא כפסק עמודי ההוראה שאין מברכין עליו לא בתחלה ולא בסוף. אם לא מפני האונס. כמ"ש בחיבורי באורך בס"ד יע"ש. וקידוש והבדלה למאן דלית ליה יין כשר. אפשר בפת ובתפלה ודיו היכא דלא אפשר:

[^2]*והנה* בי"ט שני של גליות העמידו חכמים דבריהם בביטול עשה חמור מאד. דהיינו הנחת תפילין שהמבטל הוא בכלל פושעי ישראל בגופן. ואעפ"כ דחוהו מפני גזרתם הקלה. דהא בקיאינן בקביעא דירחא. ועם היות מצות תפילין בלי ספק קודמת בזמן ובמעלה. שהיא מן התורה:

[^2] עוד הוכחות נכוחות גדולות ונאמנות לסתור דעת הנ"ל


*ומה* שהביא ממגילת תענית כנז'. כמדומה דלא ידע בצורתא דשמעתא. ואדרבה משם הי"ל להוכיח סתירת דעתו המשובשת בכאן במ"כ. שמאחר שהוצרכו להתנות עליה שהנדר הקודם יבטל את גזרתם. מכלל שאם לא התנו כך בפירוש. בסתם היה הנדר בטל בכל אופן מחמת גזרתם. ואע"פ שהנדר מן התורה. כ"ש במידי דרבנן שהם אמרו והם אמרו. ואפי' היה הנדר הקודם מבטל לגזרתם המאוחרת גם בסתם. לא הי"ל לו להביא ראיה משם כלל. דשאני התם שהנדר קודם באמת. והוא דבר של תורה. וחז"ל באו לעוקרו בקום עשה. שיש ג' הבדלים שתשב"ר מבחין בהם. שאינו דומה כלל לנ"ד וכמוכרח מהנז':

*ועוד* ראיה לסתור דעתו של זה החכם נ"ע. מהא דבעי רבא נ"ח וקידוש היום ה"מ עדיף. והדר פשטיה נ"ח עדיף משום פ"נ. ואי איתא מאי קמיבעיא ליה הא ודאי קידוש קדם בזמן לנ"ח. וכ"ש דקשיא אמסקנא היכי אתי נ"ח דמאוחר ודחי לקידוש היום דמוקדם. ומנ"ל דמהני טעם פ"נ כה"ג למידחי לתדיר ומוקדם נמי. אלא ע"כ הא לאו מילתא:

[^ שקלא וטריא בענין זה]*ואלמלא* היינו צריכין לדון אם המצוה הקודמת מבטלת להמאוחרת. יש להוכיח מהא דאי' פ"ק דחולין (דף י"ז) דמקשינן לר"ע דאמר כשהיו במדבר הותר להם בשר נחירה. מדתנן הנוחר כו' פטור מלכסות. הא קמן דס"ד דמקשן מדקדים היתר נחירה ומצות כיסוי למצות שחיטה. לא תפקיע הנחירה את הכיסוי אפי' עכשיו. ופרקינן כיון דאיתסר איתסר. ש"מ בהדיא דלא סברא היא למידחי הא מקמי הא. דא"ה דכוותה בדאורייתא נמי נימא הכי. אע"ג דהשתא איתסרא נחירה כיון דכיסוי קדים בזמן לשחיטה. לא פקע מפני מצות השחיטה. אלא ודאי דאין זו סברא באמת. דלעולם כיון דאיתסר איתסר אמרינן. דאתי בתרא ועקר לקמא לגמרי דאורייתא בדאורייתא. ודרבנן בדרבנן. אפי' בקום עשה:

[^3]*(והא* דאמרינן בריש השוחט נחירתו זוהי שחיטתו ליבעי כסוי. לאו משום דלא אלימי רבנן לאפקועיה אלא משום דבכדי ודאי לא עקרו דבר מן התורה. אע"ג דאיכא למימר דהו"ל למיגזר ביה משום הרואה שלא יאמר שזוהי שחיטתו ויבוא לאכלו. דבלא"ה נמי משני שפיר. א"נ כי אמרינן מספיקא טרח וכסי. לא חיישינן דליפוק מנה חורבא. דאפי' באיסור כרת לא חיישינן כדמסיק תלמודא בשמעתא דכוי כצואה פ"ק דביצה. כ"ש בדרבנן ודו"ק.) וכ"ש בשב ואל תעשה שגזרת חכמים עוקרת אפי' דבר מן התורה אליבא דכ"ע:

[^3] ופסק הלכה כדקיי"ל קיימא שרירא דאין מברכין על ס"י אפי' במקום מצוה אם לא מפני האונס לרפואה בדוקה היכא דלא סגי בלא"ה


*ולכן* פשוט ביותר שטעה מאד החכם בעל תשו' הנז' בזה. רחמנא ניצלן מהאי דעתא. אלא העיקר שאין מקדשין ולא מבדילין עליו. וק"ו לברכת הנאה גרידא שלא לברך עליו כלל. שאין זה מברך אלא מנאץ אף לדברי הראב"ד ז"ל. שלא נחלק להצריך ברכה על הנאת החיך. אלא באוכל ושותה דבר איסור בשוגג ובאונס. וכמ"ש בט"ז ז"ל וברור הוא. ובעל תשו' הנ"ל טעה גם בזו. ואין ספק בדבר שלד"ה אסור אפי' לברך עליו. וברור מאד יעב"ץ ס"ט:
