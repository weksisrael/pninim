#!/bin/bash
#
# An example hook script to check the commit log message.
# Called by "git commit" with one argument, the name of the file
# that has the commit message.  The hook should exit with non-zero
# status after issuing an appropriate message if it wants to stop the
# commit.  The hook is allowed to edit the commit message file.
#
# Usage:
# cp hooks/commit-msg .git/hooks/
# chmod +x .git/hooks/commit-msg
#

# Finalize message string and exit
finalize_message() {
  if (( ${#added_files[@]} + ${#deleted_files[@]} + ${#renamed_files[@]} + ${#modified_files[@]} > 1 )); then
    message="$message ..."
  fi

  echo  "$message" > "$1"
  exit 0
}

# Start with a generic message
message="update"

readarray added_files < <(git diff --name-only --staged --diff-filter=AC)
readarray deleted_files < <(git diff --name-only --staged --diff-filter=D)
readarray renamed_files < <(git diff --name-only --staged --diff-filter=R)
readarray modified_files < <(git diff --name-only --staged --diff-filter=M)

if [[ ${#added_files[@]} -gt 0 ]]; then
  message="+ ${added_files[0]}"
  message=$(echo "$message" | tr -d '\n')
  finalize_message $1
fi

if [[ ${#deleted_files[@]} -gt 0 ]]; then
  message="- ${deleted_files[0]}"
  message=$(echo "$message" | tr -d '\n')
  finalize_message $1
fi

if [[ ${#renamed_files[@]} -gt 0 ]]; then
  message="> ${renamed_files[0]}"
  message=$(echo "$message" | tr -d '\n')
  finalize_message $1
fi

if [[ ${#modified_files[@]} -gt 0 ]]; then
  message="* ${modified_files[0]}"
  message=$(echo "$message" | tr -d '\n')
  finalize_message $1
fi